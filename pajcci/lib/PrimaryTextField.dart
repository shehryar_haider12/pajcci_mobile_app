import 'package:flutter/material.dart';

import 'Constant/constFile.dart';


// ignore: must_be_immutable
class PrimaryTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? placeholder;
  IconData iconData;
  TextInputType? keyBoardType;
  final String? suffixImagePath;
  final Function? onSuffixPressed;
  final bool? obscure;

  PrimaryTextField(
      {this.controller,
      this.placeholder,
      required this.iconData,
      this.keyBoardType,
      this.suffixImagePath,
      this.obscure = false,
      this.onSuffixPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        height: 50,
        child: TextField(
          decoration: new InputDecoration(
              hintText: placeholder,
              hintStyle: PLACEHOLDER_STYLE,
              contentPadding: const EdgeInsets.symmetric(vertical: 15),
              enabled: true,
              filled: true,
              prefixIcon: Icon(iconData),
              fillColor: PRIMARY_WHITE,
              enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: PRIMARY_BORDER_COLOR, width: 1)),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: PRIMARY_BORDER_COLOR, width: 1),
              ),
              focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: PRIMARY_BORDER_COLOR, width: 1))),
        ),
      ),
    );
  }
}
