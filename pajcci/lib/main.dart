import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pajcci/Screens/Connectivity/b2bConnectivity.dart';

void main() {
  runApp(PAJCCI());
}

class PAJCCI extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: B2bConnectivity()
      //SplashScreen(),
    );
  }
}
