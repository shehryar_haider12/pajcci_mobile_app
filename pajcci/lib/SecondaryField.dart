import 'package:flutter/material.dart';

import 'Constant/constFile.dart';


// ignore: must_be_immutable
class SecondaryField extends StatelessWidget {
  final TextEditingController? controller;
  final String? placeholder;
  IconData prefixIcon;
  TextInputType? keyBoardType;
  final Widget? suffixIcon;
  final Function? onSuffixPressed;
  final bool? obscure;
  final bool isVisible;

  SecondaryField(
      {this.controller,
      this.placeholder,
      required this.isVisible,
      required this.prefixIcon,
      this.keyBoardType,
      this.suffixIcon,
      this.obscure = false,
      this.onSuffixPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        height: 50,
        decoration: const BoxDecoration(boxShadow: [
          BoxShadow(
              offset: Offset(10, 10),
              blurRadius: 20,
              spreadRadius: 10,
              color: Colors.black12)
        ]),
        child: TextField(
          decoration: InputDecoration(
              hintText: placeholder,
              hintStyle: PLACEHOLDER_STYLE,
              contentPadding: const EdgeInsets.symmetric(vertical: 15),
              enabled: true,
              filled: true,
              suffixIcon: suffixIcon,
              prefixIcon: Icon(prefixIcon),
              fillColor: PRIMARY_WHITE,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: PRIMARY_WHITE, width: 1)),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: PRIMARY_WHITE, width: 1),
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: PRIMARY_WHITE, width: 1))),
        ),
      ),
    );
  }
}
