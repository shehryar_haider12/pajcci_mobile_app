import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pajcci/Constant/constFile.dart';
import 'package:pajcci/Screens/SplashScreen/SplashController/SplashController.dart';

// ignore: must_be_immutable
class SplashScreen extends StatelessWidget {
  SplashScreenController _splashScreenController =
      Get.put(SplashScreenController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_GREEN,
      body: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: Get.height,
            child: Image.asset(
              IMAGE_PATH + "backImage1.png",
              // height: Get.height,
              fit: BoxFit.fitHeight,
            ),
          ),
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  IMAGE_PATH + 'logoSmall.png',
                  fit: BoxFit.cover,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Text(
                    LOGO_TITLE,
                    style: TextStyle(
                        color: PRIMARY_WHITE,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
