

import 'package:get/get.dart';
import 'package:pajcci/Screens/Authentication/Authenication.dart';

class SplashScreenController extends GetxController {
  void nextScreen() {
    Future.delayed(Duration(seconds: 3), () {
      Get.off(()=> AuthenticationScreen());
    });
  }

  void onInit() {
    super.onInit();
    nextScreen();
  }
}
