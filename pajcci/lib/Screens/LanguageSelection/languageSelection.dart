import 'package:flutter/material.dart';
import 'package:pajcci/Constant/constFile.dart';
import 'package:pajcci/Screens/Authentication/LogoHeader.dart';
import 'package:pajcci/Widgets/CommonWidget.dart';

// ignore: must_be_immutable
class LanguageSelection extends StatelessWidget {
  final List<String> languagesList = [
    'English',
    'Urdu',
    'Japnesses',
    'Bengali',
    'Russian',
    'Telugu',
    'Yue Chinese',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_WHITE,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          LogoHeader(),
          //! Two Button Align Center
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14),
                          border: Border.all(color: Colors.black12),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: languageDropDown()),
                CommonWidget().sizedBox(10.0, 0.0),
                CommonWidget().primaryButtonWidget(
                  () {},
                  'Submit',
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  // Widget languageDropDown() {
  //   return DropdownButton(
  //     elevation: 3,
  //       hint: Text('Select Language'),
  //        isExpanded: false,
  //       // decoration: InputDecoration(
  //       //     contentPadding: EdgeInsets.all(1), border: InputBorder.none),
  //      // value: _currentcjmStatus, //shows the selected value
  //       items: languagesList.map((e) {
  //         //e is varaible for list sugar define above
  //         return DropdownMenuItem(
  //           child: Text('$e'),
  //           value: e,
  //         );
  //       }).toList(),
  //       onChanged: (val) {
  //         print(val);
  //       });
  // }
  Widget languageDropDown() {
    return DropdownButtonFormField(
       isExpanded: true,
      elevation: 3,
        hint: Text('Select Language'),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.all(1), border: InputBorder.none),
       // value: _currentcjmStatus, //shows the selected value
        items: languagesList.map((e) {
          //e is varaible for list sugar define above
          return DropdownMenuItem(
            child: Text('$e'),
            value: e,
          );
        }).toList(),
        onChanged: (val) {
          print(val);
        });
  }
}
