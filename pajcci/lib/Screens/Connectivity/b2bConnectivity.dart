import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pajcci/Constant/constFile.dart';
import 'package:pajcci/Screens/Connectivity/ConnectivityController/connectivityController.dart';
import 'package:pajcci/SecondaryField.dart';
import 'package:pajcci/Widgets/CommonWidget.dart';
import 'package:carousel_slider/carousel_slider.dart';

// ignore: must_be_immutable
class B2bConnectivity extends StatelessWidget {
  TextEditingController search = TextEditingController();
  final connectivityController = Get.put(ConnectivityController());
//!
  CarouselSlider? carouselSlider;

  List imgList = [
    'assets/icons/backImage1.png',
    'assets/icons/backImage1.png',
    'assets/icons/backImage1.png',
    'assets/icons/backImage1.png',
    'assets/icons/backImage1.png',
  ];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

//!
  final globalKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: globalKey,
        drawer: Drawer(),
        backgroundColor: PRIMARY_WHITE,
        body: ListView(
          children: [
            //! Header
            header(),
            CommonWidget().sizedBox(20.0, 0.0),
            //! Search Field
            SecondaryField(
              controller: search,
              prefixIcon: Icons.search,
              suffixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: new BoxDecoration(
                    color: PRIMARY_GREEN,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Icon(
                    Icons.checklist,
                    color: PRIMARY_WHITE,
                  ),
                ),
              ),
              placeholder: 'Title or Description',
              isVisible: true,
            ),
            CommonWidget().sizedBox(20.0, 0.0),
            //! Slider
            curosolSlider(),
            //!
            Center(
              child: CommonWidget().text('Top Business Profiles', 20.0,
                  PRIMARY_BLACK, FontWeight.w500, 1),
            ),
            //!
            CommonWidget().sizedBox(10.0, 0.0),
            //! Partner Container
            Container(
              height: 100,
              margin: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                border: Border.all(color: Colors.black12),
              ),
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 6,
                  itemBuilder: (index, context) {
                    return FlutterLogo(
                      size: 100,
                    );
                  }),
            ),
            CommonWidget().sizedBox(10.0, 0.0),
            //! TabView Nested
            Container(
                height: 200,
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 5),
                child: Card(
                  elevation: 5,
                  child: Column(children: [
                    TabBar(
                        controller: connectivityController.tabControllerFirst,
                        indicatorColor: PRIMARY_GREEN,
                        indicatorWeight: 2.5,
                        tabs: connectivityController.tabsFirst),
                    Expanded(
                      child: TabBarView(
                        controller: connectivityController.tabControllerFirst,
                        children: <Widget>[
                          //!----------New
                          Text('data 1'),
                          //!----------Business Opportunity
                          Text('data 2'),
                          //!----------Events
                          ListView.builder(
                              itemCount: 3,
                              itemBuilder: (context, index) {
                                return events();
                              })
                        ],
                      ),
                    ),
                  ]),
                )),
                //! Tab View videos/Advertisment
            Container(
                height: 200,
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 5),
                child: Column(children: [
                  TabBar(
                      controller: connectivityController.tabControllerSecond,
                      indicatorColor: PRIMARY_GREEN,
                      indicatorWeight: 2.5,
                      tabs: connectivityController.tabsSecond),
                  Expanded(
                    child: TabBarView(
                      controller: connectivityController.tabControllerSecond,
                      children: <Widget>[
                        //!----------New
                        Text('data 1'),
                        //!----------Business Opportunity
                        Text('data 2'),
                        
                      ],
                    ),
                  ),
                ])),
          ],
        ),
      ),
    );
  }

//! Header ----------------------------------
  header() {
    return Container(
      height: Get.height * 0.21,
      decoration: const BoxDecoration(
          color: PRIMARY_GREEN,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(30),
            bottomLeft: Radius.circular(30),
          )),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            //! Back Button
            GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Icon(
                  Icons.arrow_back,
                  color: PRIMARY_WHITE,
                  size: 27,
                )),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    IMAGE_PATH + "logoSmall.png",
                    fit: BoxFit.cover,
                    height: 50,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      LOGO_TITLE,
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  ),
                ],
              ),
            ),
            //! Drawer Button
            GestureDetector(
              onTap: () {
                globalKey.currentState!.openDrawer();
              },
              child: Icon(
                Icons.dehaze,
                color: PRIMARY_WHITE,
                size: 36,
              ),
            )
          ],
        ),
      ),
    );
  }

  //! Curosol Slider--------------------------------
  Widget curosolSlider() {
    return Column(
      children: [
        CarouselSlider(
          options: CarouselOptions(
            viewportFraction: 1.1,
            height: 200.0,
            autoPlay: false,
            enlargeCenterPage: true,
            onPageChanged: (index, reason) {
              connectivityController.updateIndex(index);
            },
          ),
          items: imgList.map((i) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    decoration: BoxDecoration(color: Colors.amber),
                    child: Image.asset(i));
              },
            );
          }).toList(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: map<Widget>(imgList, (index, url) {
            return Obx(() => Container(
                  width: 10.0,
                  height: 10.0,
                  margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.green),
                    color: connectivityController.currentIndex == index
                        ? Colors.green
                        : Colors.white.withOpacity(0.5),
                  ),
                ));
          }),
        ),
      ],
    );
  }
  
  //! Events Tab View
  Widget events() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5,vertical: 5),
      child: Row(
        children: [
          //! Image Container
          Expanded(
            flex: 2,
            child: Container(
              height: 70,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: PRIMARY_DARK_GREY),
              child: Text('data'),
            ),
          ),
          CommonWidget().sizedBox(0.0, 8.0),
          //! Title--- ----------------
          Expanded(
            flex: 4,
            child: Padding(
              padding: const EdgeInsets.only(right: 6),
              child: Text(
                'names[index]db fdhgdf fshgdfb fsgdfff sfds,',
                style: TextStyle(color: Colors.black, fontSize: 17),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
          )
        ],
      ),
    );
  }
}
