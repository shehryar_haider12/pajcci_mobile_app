import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pajcci/Constant/constFile.dart';
import 'package:pajcci/Widgets/CommonWidget.dart';

class ConnectivityController extends GetxController
    with SingleGetTickerProviderMixin {
  RxInt currentIndex = 0.obs;

  TabController? tabControllerFirst;
  TabController? tabControllerSecond;

  var tabsFirst = [
    CommonWidget().text('New', 15.0, PRIMARY_BLACK,FontWeight.normal, 1),
    CommonWidget().text('Business opportunities', 13.5, PRIMARY_BLACK,FontWeight.normal, 4),
    CommonWidget().text('Events', 15.0, PRIMARY_BLACK,FontWeight.normal, 1),
  ];
  var tabsSecond = [
    CommonWidget().text('Videos', 16.0, PRIMARY_BLACK,FontWeight.normal, 1),
    CommonWidget().text('Advertisement', 16.0, PRIMARY_BLACK,FontWeight.normal, 1),
  ];

  @override
  void onInit() {
    super.onInit();
    tabControllerFirst = TabController(length: 3, vsync: this);
    tabControllerSecond = TabController(length: 2, vsync: this);
  }

  updateIndex(index) {
    currentIndex.value = index;
    update();
  }
   @override
  void onClose() {
    tabControllerFirst!.dispose();
    super.onClose();
  }
}
