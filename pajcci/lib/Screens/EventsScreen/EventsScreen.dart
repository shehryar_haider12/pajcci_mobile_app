import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pajcci/Constant/constFile.dart';
import 'package:pajcci/SecondaryField.dart';
import 'package:pajcci/Widgets/CommonWidget.dart';



// ignore: must_be_immutable
class EventsScreen extends StatelessWidget {
  TextEditingController search = TextEditingController();
  TextEditingController location = TextEditingController();
  TextEditingController date = TextEditingController();

  List<String> names = [
    'Afghanistan international Trade Fair',
    'Afghanistan international Trade Fair',
    'Afghanistan international Trade Fair',
    'Afghanistan international Trade Fair',
    'Afghanistan international Trade Fair',
  ];
  final globalKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: globalKey,
        drawer: Drawer(),
        backgroundColor: PRIMARY_WHITE,
        body: SingleChildScrollView(
          child: Column(
            children: [
              //! Header 
              header(),
              CommonWidget().sizedBox(20.0,0.0),
              //! Event Header 
              eventHeading(),
              CommonWidget().sizedBox(25.0,0.0),
              //! Fields 
              SecondaryField(
                controller: search,
                prefixIcon: Icons.search,
                suffixIcon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: new BoxDecoration(
                      color: PRIMARY_GREEN,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Icon(
                      Icons.checklist,
                      color: PRIMARY_WHITE,
                    ),
                  ),
                ),
                placeholder: 'Title or Description',
                isVisible: true,
              ),
              CommonWidget().sizedBox(20.0,0.0),
              SecondaryField(
                controller: search,
                prefixIcon: Icons.location_on_outlined,
                placeholder: 'Location',
                isVisible: false,
                suffixIcon: Icon(
                  Icons.keyboard_arrow_down_rounded,
                  color: PRIMARY_DARK_GREY,
                  size: 35,
                ),
              ),
               CommonWidget().sizedBox(20.0,0.0),
              SecondaryField(
                controller: search,
                isVisible: false,
                prefixIcon: Icons.event_available,
                placeholder: 'Event Year',
              ),
              CommonWidget().sizedBox(20.0,0.0),
              //! Search Button
              CommonWidget().primaryButtonWidget(() {}, 'Search',),
              CommonWidget().sizedBox(20.0,0.0),
              //! Container With Image & Title
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: ListView.separated(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Container(
                        height: Get.height * 0.16,
                        decoration: new BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          border: Border.all(color: Colors.black12),
                          color: PRIMARY_WHITE,
                        ),
                        child: Row(
                          children: [
                             //! Image Container
                            Expanded(
                              flex: 2,
                              child: Container(
                                
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: PRIMARY_DARK_GREY),
                              ),
                            ),
                            CommonWidget().sizedBox(0.0,8.0),
                            //! Title--- Rating + share + Like--------------
                            Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 6),
                                    child: Text(
                                      names[index],
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 17),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8),
                                        //! Rating + share + Like--------------
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        SvgPicture.asset(
                                          IMAGE_PATH + 'rating.svg',
                                          color: Colors.red,
                                        ),
                                        SvgPicture.asset(
                                          IMAGE_PATH + 'handIcon.svg',
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        height: 15,
                      );
                    },
                    itemCount: names.length),
              ),
              CommonWidget().sizedBox(20.0,0.0),
              //! Page Numbers & Next
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    paginationButton(
                        color: PRIMARY_GREEN,
                        child: Text(
                          '1',
                          style: TextStyle(color: Colors.white),
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: paginationButton(
                        color: PRIMARY_WHITE,
                        borderColor: PRIMARY_GREEN,
                        child: Text(
                          '2',
                          style: TextStyle(color: PRIMARY_GREEN),
                        ),
                      ),
                    ),
                    paginationButton(
                      color: PRIMARY_WHITE,
                      borderColor: PRIMARY_GREEN,
                      child: Text(
                        'Next',
                        style: TextStyle(color: PRIMARY_GREEN),
                      ),
                    ),
                  ],
                ),
              ),
              CommonWidget().sizedBox(20.0,0.0),
            ],
          ),
        ),
      ),
    );
  }


//! paginationButton ----------------------------------
  paginationButton({color, child, borderColor = PRIMARY_GREEN}) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: borderColor),
        color: color,
      ),
      child: child,
    );
  }

//! Header ----------------------------------
  header() {  
    return Container(
      height: Get.height * 0.21,
      decoration: const BoxDecoration(
          color: PRIMARY_GREEN,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(30),
            bottomLeft: Radius.circular(30),
          )),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            //! Back Button
            GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Icon(
                  Icons.arrow_back,
                  color: PRIMARY_WHITE,
                  size: 27,
                )),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    IMAGE_PATH + "logoSmall.png",
                    fit: BoxFit.cover,
                    height: 50,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      LOGO_TITLE,
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  ),
                ],
              ),
            ),
             //! Drawer Button
            GestureDetector(
              onTap: () {
                globalKey.currentState!.openDrawer();
              },
              child: Icon(
                Icons.dehaze,
                color: PRIMARY_WHITE,
                size: 36,
              ),
            )
          ],
        ),
      ),
    );
  }

//! Event Header ----------------------------------
  eventHeading() {
    return Column(
      children: [
        Text(
          'Events',
          style: TextStyle(
              color: PRIMARY_GREEN, fontWeight: FontWeight.bold, fontSize: 35),
        ),
        SizedBox(
          height: 4,
        ),
        Container(
          height: 2.5,
          width: 60,
          color: PRIMARY_GREEN,
        ),
        SizedBox(
          height: 3,
        ),
        Container(
          height: 2.5,
          width: 40,
          color: PRIMARY_GREEN,
        ),
      ],
    );
  }
}
