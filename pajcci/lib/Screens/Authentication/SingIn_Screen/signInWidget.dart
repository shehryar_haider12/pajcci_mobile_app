import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pajcci/Screens/EventsScreen/EventsScreen.dart';
import 'package:pajcci/Widgets/CommonWidget.dart';
import '../../../Constant/constFile.dart';
import '../../../PrimaryTextField.dart';

class SignInWidget extends StatelessWidget {
  final TextEditingController userName = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController confirmPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_WHITE,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: Get.height * 0.1,
              ),
              PrimaryTextField(
                controller: userName,
                iconData: Icons.perm_identity_sharp,
                placeholder: "Username",
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 20),
                child: PrimaryTextField(
                  controller: userName,
                  placeholder: "Password",
                  iconData: Icons.lock_open_rounded,
                ),
              ),
              SizedBox(
                height: Get.height * 0.09,
              ),
               CommonWidget().primaryButtonWidget(
                
                 () {
                   Get.to(()=>EventsScreen());
                  print('sign In clicked');
                },'Sign In',
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  'Forgot Password?',
                  style: TextStyle(
                    color: Colors.black38,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
