import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pajcci/PrimaryTextField.dart';
import 'package:pajcci/Screens/LanguageSelection/languageSelection.dart';
import 'package:pajcci/Widgets/CommonWidget.dart';

import '../../../Constant/constFile.dart';

// ignore: must_be_immutable
class SignUpWidget extends StatelessWidget {
  
  TextEditingController userName = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_WHITE,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 40,
              ),
              PrimaryTextField(
                controller: userName,
                iconData: Icons.perm_identity_sharp,
                placeholder: "Username",
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 20),
                child: PrimaryTextField(
                  controller: userName,
                  placeholder: "Password",
                  iconData: Icons.lock_open_rounded,
                ),
              ),
              PrimaryTextField(
                controller: userName,
                placeholder: "Confirm Password",
                iconData: Icons.lock_open_rounded,
              ),
              SizedBox(
                height: Get.height * 0.09,
              ),
               CommonWidget().primaryButtonWidget(
                
                 () {
                   Get.to(()=>LanguageSelection());
                  print('signup clicked');
                },'Sign Up',
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    // Get.back();
                  },
                  child: Text(
                    'Already Have an account?',
                    style: TextStyle(
                      color: Colors.black38,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
