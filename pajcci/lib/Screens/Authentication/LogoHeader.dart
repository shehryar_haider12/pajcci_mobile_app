import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pajcci/Constant/constFile.dart';

class LogoHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.red,
      height: Get.height * 0.35,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Align(
              alignment: Alignment.topCenter,
              child: Image.asset(
                IMAGE_PATH + 'logoSmall.png',
                fit: BoxFit.cover,
                height: Get.height * 0.15,
              ),
            ),
          ),
          Text(
            LOGO_TITLE,
            style: HEADING_STYLE,
          ),
        ],
      ),
    );
  }
}
