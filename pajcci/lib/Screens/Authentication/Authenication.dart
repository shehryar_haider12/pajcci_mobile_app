import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pajcci/Constant/constFile.dart';
import 'package:pajcci/Screens/Authentication/LogoHeader.dart';
import 'package:pajcci/Widgets/CommonWidget.dart';
import 'ToggleAuthentication/ToggleAuthenticatiion.dart';

// ignore: must_be_immutable
class AuthenticationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_WHITE,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          LogoHeader(),
          //! Two Button Align Center
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CommonWidget().primaryButtonWidget(
                  () {
                    Get.to(() => ToggleAuthentication(
                          initialIndex: 0,
                        ));},
                  'Sign in',
                ),
                CommonWidget().sizedBox(10.0, 0.0),
                CommonWidget().primaryButtonWidget(
                  () {
                    Get.to(() => ToggleAuthentication(
                          initialIndex: 1,
                        ));
                  },
                  'Sign up',
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
