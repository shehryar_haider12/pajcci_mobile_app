import 'package:flutter/material.dart';
import 'package:pajcci/Constant/constFile.dart';
import 'package:pajcci/Screens/Authentication/LogoHeader.dart';
import 'package:pajcci/Screens/Authentication/SignUp_Screen/SignupWidget.dart';
import 'package:pajcci/Screens/Authentication/SingIn_Screen/signInWidget.dart';


// ignore: must_be_immutable
class ToggleAuthentication extends StatelessWidget {

  final int initialIndex;
  
  ToggleAuthentication({required this.initialIndex});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_WHITE,
      body: Column(
        children: [
          LogoHeader(),
          Expanded(
              child: TabView(
            initialIndex: initialIndex,
          )),
        ],
      ),
    );
  }
}


class TabView extends StatelessWidget {
  final int initialIndex;
  TabView({this.initialIndex = 0});
  
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: initialIndex,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          leading: IgnorePointer(
              ignoring: true,
              child: Icon(
                Icons.arrow_back,
                color: Colors.transparent,
              )),
          toolbarHeight: 80,
          elevation: 0,
          backgroundColor: PRIMARY_WHITE,
          // title: const Text('TabBar Widget'),
          bottom: const TabBar(
            indicatorColor: PRIMARY_GREEN,
            indicatorWeight: 2.5,
            tabs: <Widget>[
              Tab(
                child: Text(
                  'Sign In',
                  style: TAB_STYLE,
                ),
              ),
              Tab(
                child: Text(
                  'Sign Up',
                  style: TAB_STYLE,
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            SignInWidget(),
            SignUpWidget(),
          ],
        ),
      ),
    );
  }
}