import 'package:flutter/material.dart';

const HEADING_STYLE = const TextStyle(
    color: Colors.black, fontSize: 19, fontWeight: FontWeight.bold);
const TAB_STYLE = const TextStyle(
    color: Colors.black, fontSize: 19, fontWeight: FontWeight.normal);
const PLACEHOLDER_STYLE = const TextStyle(
    color: Colors.black26, fontSize: 17, fontWeight: FontWeight.normal);
const IMAGE_PATH = 'assets/icons/';
const PRIMARY_GREEN = Color(0xff01853C);
const PRIMARY_WHITE = Color(0xffFFFFFF);
const PRIMARY_GREY = Color(0xff00000012);
const PRIMARY_BLACK = Color(0xff000000);
const PRIMARY_DARK_GREY = Color(0xffE1E1E1);
const PRIMARY_LIGHT_BLACK = Color(0xff545454);
const PRIMARY_BORDER_COLOR = Color(0xffC1C1C1);

const LOGO_TITLE = 'PAJCCI Business Portal';
