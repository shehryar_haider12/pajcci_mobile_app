import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pajcci/Constant/constFile.dart';

class CommonWidget {
  //! Sized BOx Widget--------------------------------
  Widget sizedBox(double height,double width) {
    return SizedBox(
      height: height,
      width: width,
    );
  }
//! Green Elevated Button---------------------------------
Widget primaryButtonWidget(onPressed,value){
  return Container(
     width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: ElevatedButton(
          // splashColor: Colors.white,
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(primary: PRIMARY_GREEN, 
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
            side: BorderSide(
              color: PRIMARY_GREEN,
              width: 1,
            ),
          ),),
          //color: PRIMARY_GREEN,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 12),
            child: Text(
              value.toString(),
              textScaleFactor: 1.2,
              style: TextStyle(
                  color: PRIMARY_WHITE, fontFamily: 'Josefin Sans, Regular'),
            ),
          ),
         
        ),
      ),
    );
}

//! text----------------------------------
 Text text(
    text,
    fontSize,
    color,
    fontWeight,
    maxlines,
  ) {
    return Text(
      text,
      maxLines: maxlines,
      overflow: TextOverflow.ellipsis,
      style:
          TextStyle(fontSize: fontSize, color: color, fontWeight: fontWeight),
    );
  }

}
